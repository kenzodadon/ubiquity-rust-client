# BalanceChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**old** | Option<**String**> | Balance before transaction | [optional]
**delta** | Option<**String**> | Balance difference | [optional]
**new** | Option<**String**> | Balance after transaction | [optional]
**currency** | Option<[**crate::models::Currency**](currency.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


