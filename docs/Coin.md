# Coin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | Option<**String**> | Platform handle (platform var in path) | [optional]
**slip44** | Option<**i32**> | SatoshiLabs 0044, registered coin types: https://github.com/satoshilabs/slips/blob/master/slip-0044.md | [optional]
**symbol** | Option<**String**> | Symbol of native currency | [optional]
**name** | Option<**String**> | Name of platform | [optional]
**block_time** | Option<**i32**> | Average time between blocks (milliseconds) | [optional]
**sample_address** | Option<**String**> | Random address seen on chain (optional) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


